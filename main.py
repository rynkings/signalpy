from signalpy import Signal
from pathlib import Path

import re
import time
import os
import json
import requests
import random

client = Signal('qr')
client.login_qr('Ryns', 'session')

os.system('rm -rf /root/.config/signald/data/' + client.username + '/msg-chache') #replace with your signald config

def downloadYoutube(url, tp):
    if tp == 'video':
        video_path = Path(__file__).parent.absolute() / "tmp" / "tmp.mp4"
        if os.path.exists(video_path):
            os.remove(video_path)
        os.system("youtube-dl -f best -o tmp/tmp.mp4 " + url)
        return Path(__file__).parent.absolute() / "tmp" / "tmp.mp4"
    elif tp == 'audio':
        audio_path = Path(__file__).parent.absolute() / "tmp" / "tmp.mp3"
        if os.path.exists(audio_path):
            os.remove(audio_path)
        os.system('youtube-dl -f bestaudio -o tmp/tmp.mp3 ' + url)
        return Path(__file__).parent.absolute() / "tmp" / "tmp.mp3"

@client.handle_chat(msg_print=True)
def handleMessages(message):
    print(message)
    text = message.text
    sender = message.sender
    to = message.to
    txt = str(text).lower()

    reacts = '😀,😃,😄,😁,😆,😅,🤣,😂,🙂,🙃,😉,😊,😇,🥰,😍,🤩,💩,🤡,❤️,💓,💕,💋,👌,🖐️,💨,💦'.split(',')
    client.sendReact(to, random.choice(reacts), sender, message.timestamp)

    if txt == 'hi':
        client.sendMessage(to, 'Hi too!!')
    elif txt == 'sp' or txt == 'speed':
        start = time.time()
        client.sendMessage(to, "Checking speed ...")
        elapsed_time = time.time() - start
        client.sendMessage(to, 'kecepatan mengirim pesan...\n{}'.format(str(elapsed_time)))
    elif txt.startswith("ytmp3 "):
        sep = text.split(" ")
        txt = text.replace(sep[0] + " ","")
        path = downloadYoutube(txt, 'audio')
        client.sendMessage(to, 'tunggu...')
        client.sendMedia(to, path, 'KNTL')
        os.remove(path)
    elif txt.startswith("ytmp4 "):
        sep = text.split(" ")
        txt = text.replace(sep[0] + " ","")
        path = downloadYoutube(txt, 'video')
        client.sendMessage(to, 'tunggu...')
        client.sendMedia(to, path, 'KNTL')
        os.remove(path)
    elif txt.startswith("yt "):
        sep = text.split(" ")
        query = text.replace(sep[0] + " ","")
        cond = query.split("|")
        search = str(cond[0])
        r = requests.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q={}&type=video&key=AIzaSyBgLiO8GoVAXggNkuT9Ps0wjjgsBCuRTrw".format(search))
        data = r.text
        data = json.loads(data)
        #print(str(data))
        if len(cond) == 1:
            num = 0
            aa = "* Youtube Search*\n\n"
            #z = "https://www.youtube.com/watch?v={}".format(music["id"]["videoId"])
            for music in data["items"]:
                num += 1
                aa += "\n{}\n{} ({})".format(music["snippet"]["title"],"https://www.youtube.com/watch?v="+music["id"]["videoId"],num)
            aa += "\nTotal {} ".format(str(len(music)))
            aa += "\n\nUntuk Melihat Details Youtube, silahkan gunakan command yt {}|「number」".format(search)
            client.sendMessage(to, aa)
        elif len(cond) == 2:
            num = int(cond[1])
            if num <= len(data):
                music = data["items"][num - 1]
                aa = "*Youtube*\n"
                aa += "\n⋄ Title: {}".format(music["snippet"]["title"])
                aa += "\n⋄ Channel: {}".format(music["snippet"]["channelTitle"])
                aa += "\n⋄ Desc: {}".format(music["snippet"]["description"])
                aa += "\n⋄ Publih: {}".format(music["snippet"]["publishTime"])
                aa += "\n⋄ URL: https://www.youtube.com/watch?v={}".format(music["id"]["videoId"])
                url = "https://www.youtube.com/watch?v={}".format(music["id"]["videoId"])
                path = downloadYoutube(url, 'video')
                apath = downloadYoutube(url, 'audio')
                client.sendMedia(to, path)
                client.sendMedia(to, apath, caption=aa)

client.run()
