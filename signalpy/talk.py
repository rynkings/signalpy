def QuoteContext(idx, author, text):
    return {"id": idx, "author": author, "text": text}

def EmojiContext(emoji, number, idx):
    return {"emoji": emoji, "targetAuthor": {"number": number}, "targetSentTimestamp": idx}

class Talk(object):

    def sendMessage(self, to, text, context={}):
        payload = {
            "type": "send",
            "username": self.username,
            "messageBody": text
        }
        if to.startswith('+'):
            payload['recipientAddress'] = to
        else:
            payload['recipientGroupId'] = to
        if context:
            payload['quote'] = context
        self._send_command(payload, True)

    def sendReact(self, to, react, target_number, idx):
        payload = {"type": "react", "reaction": EmojiContext(react, target_number, idx), "username": self.username}
        if to.startswith('+'):
            payload['recipientAddress'] = to
        else:
            payload['recipientGroupId'] = to
        self._send_command(payload, True)

    def sendMedia(self, to, path, caption='', context=None):
        payload = {
        	"type": "send",
        	"username": self.username,
        	"messageBody": caption,
        	"attachments": [{"filename": str(path), "width": "100", "height": "100"}]
        }
        if to.startswith('+'):
            payload['recipientAddress'] = to
        else:
            payload['recipientGroupId'] = to
        if context:
            payload['quote'] = context
        self._send_command(payload, True)

    def sendMention(self, to, text, mentions=[], context={}): #still not working
    	payload = {
    		'type': 'send',
    		'username': self.username,
    		'messageBody': text.replace('@!', '￼'),
            'quote': {
                'mentions': []
            }
    	}
    	for mention in mentions:
    		payload['quote']['mentions'].append({'uuid': mention, 'length': 1, 'start': 1})
    	if context:
    		payload['quote'].update(context)
    	print('Payload :', payload)
    	self._send_command(payload, True)

    def fake_message(self, to, target, text1, text2):
        context = QuoteContext(1610419492077, target, text2)
        return self.sendMessage(to, text1, context)