import json
import random
import re
import socket
import pyqrcode
import os
import time

from typing import Iterator, List

from .types import Attachment, Message, Group, GroupV2, Quote
from .talk import Talk

# We'll need to know the compiled RE object later.
RE_TYPE = type(re.compile(""))


def readlines(s: socket.socket) -> Iterator[bytes]:
    "Read a socket, line by line."
    buf = []  # type: List[bytes]
    while True:
        char = s.recv(1)
        if not char:
            raise ConnectionResetError("connection was reset")

        if char == b"\n":
            yield b"".join(buf)
            buf = []
        else:
            buf.append(char)


class Signal(Talk):
    def __init__(self, username, socket_path="/var/run/signald/signald.sock"):
        self.username = username
        self.socket_path = socket_path
        self._chat_handlers = []
        self._handle_messages = []
        self.start_time = time.time()

    def _get_id(self):
        "Generate a random ID."
        return "".join(random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for _ in range(10))

    def _get_socket(self) -> socket.socket:
        "Create a socket, connect to the server and return it."

        # Support TCP sockets on the sly.
        if isinstance(self.socket_path, tuple):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(self.socket_path)
        return s

    def _send_command(self, payload: dict, block: bool = False):
        s = self._get_socket()
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("utf8") + b"\n")

        if not block:
            return

        response = s.recv(4 * 1024)
        for line in response.split(b"\n"):
            if msg_id.encode("utf8") not in line:
                continue

            data = json.loads(line)

            if data.get("id") != msg_id:
                continue

            if data["type"] == "unexpected_error":
                raise ValueError("unexpected error occurred")

    def register(self, voice=False):
        """
        Register the given number.

        voice: Whether to receive a voice call or an SMS for verification.
        """
        payload = {"type": "register", "username": self.username, "voice": voice}
        self._send_command(payload)

    def verify(self, code: str):
        """
        Verify the given number by entering the code you received.

        code: The code Signal sent you.
        """
        payload = {"type": "verify", "username": self.username, "code": code}
        self._send_command(payload)

    def login_qr(self, qrname, session_name):
        if os.path.exists(session_name + '.ryns'):
            with open(session_name + '.ryns', 'r') as session:
                session = json.loads(session.read())
                if session['username'] in os.listdir('/root/.config/signald/data/'):
                    return True

        payload = {"type": "link", "deviceName": qrname}
        s = self._get_socket()
        s.send(json.dumps(payload).encode("utf8") + b"\n")

        for line in readlines(s):
            try:
                message = json.loads(line.decode())
            except json.JSONDecodeError:
                print("Invalid JSON")
            if message['data'].get('uri'):
                qr = pyqrcode.create(message['data']['uri'])
                qr.png('qr.png', scale=6, module_color=[0, 0, 0, 128], background=[0xff, 0xff, 0xcc])
                print('Please scan the qr\n', message['data']['uri'])
            if message['type'] == 'linking_successful':
                self.username = message['data'].get('username')
                with open(session_name + '.ryns', 'w') as session:
                    session.write(json.dumps({"username": message['data']['username']}))
                    return True
            elif message['type'] == 'linking_error':
                print('Error found :\n' + str(message['data'].get('message')))
                inp = input('Token is revoked ? (y/n): ')
                if str(inp).strip() == 'y':
                    os.system('rm -rf /root/.config/signald/data/+' + re.search(r'\d+', str(message['data']['message'])).group(0))
                    return self.login_qr(qrname, session_name)
                else:
                    self.username = '+' + str(re.search(r'\d+', str(message['data']['message'])).group(0))
                    with open(session_name + '.ryns', 'w') as session:
                        session.write(json.dumps({"username": self.username}))
                        return True

    def serializeMessages(self, messages):
        print(messages)
        msg = Message()
        if messages.get('type') == 'message':
            data = messages['data']
            msg.type = messages['type']
            msg.username = data['username']
            msg.source = data['source']
            msg.sender = data['source'].get('number')
            msg.sender_uuid = data['source'].get('uuid')
            msg.has_content = data.get('hasContent', False)
            if data.get("syncMessage"):
                sm = data.get('syncMessage')
                msg.client_type = 0
                if sm.get('sent'):
                    msg.text = sm['sent']['message'].get('body')
                    msg.timestamp = sm['sent']['message'].get('timestamp')
                    msg.timestamp_iso = data['timestampISO']
                    msg.expiration_secs = sm.get('expiresInSeconds')
                    msg.group_info = sm.get('groupInfo', {})
                    groupV2 = sm['sent']['message'].get('groupV2', {})
                    if groupV2:
                        groupV2 = GroupV2(
                            group_id=groupV2.get('id')
                        )
                    isGroup = True if groupV2 != None else False
                    to = msg.sender if not isGroup else groupV2.group_id
                    attachments = sm['sent']['message'].get('attachments', [])
                    msg.to = to
                    msg.isGroup = isGroup
                    if sm['sent']['message'].get('quote', {}):
                        msg.quote = Quote(
                            id=sm['sent']['message']['quote']['id'],
                            author=sm['sent']['message']['quote']['author']['uuid'],
                            text=sm['sent']['message']['quote']['text']
                        )
                    msg.attachments = [
                        Attachment(
                            content_type=attachment['contentType'],
                            id=attachment['id'],
                            size=attachment['size'],
                            stored_filename=attachment['storedFilename']
                        )
                        for attachment in attachments
                    ]
            elif data.get('dataMessage'):
                message = messages['data']
                data_message = message.get("dataMessage", {})
                group = data_message.get('group')
                groupV2 = data_message.get('groupV2')
                quote = data_message.get('quote')
                if group:
                    group = Group(
                        group_id=data_message["group"].get("groupId"),
                        group_name=data_message["group"].get("name"),
                        group_type=data_message["group"].get("type")
                    )
                if groupV2:
                    groupV2 = GroupV2(
                        group_id=data_message['groupV2'].get('id')
                    )
                if quote:
                    quote = Quote(
                        id=data_message['quote'].get('id'),
                        author=data_message['quote'].get('author').get('uuid'),
                        text=data_message['quote'].get('text')
                    )
                isGroup = True if group != None or groupV2 != None else False
                to = message['source']['number'] if not isGroup else groupV2.group_id or group.group_id

                msg =  Message(
                    username=message["username"],
                    source=message["source"],
                    sender=message['source']['number'],
                    sender_uuid = message['source']['uuid'],
                    to=to,
                    client_type=1,
                    text=data_message.get("body"),
                    source_device=message["sourceDevice"],
                    timestamp=data_message.get("timestamp"),
                    timestamp_iso=message["timestampISO"],
                    expiration_secs=data_message.get("expiresInSeconds"),
                    group_info=data_message.get("groupInfo", {}),
                    isGroup=isGroup,
                    group=group,
                    groupV2=groupV2,
                    quote=quote,
                    attachments=[
                        Attachment(
                            content_type=attachment["contentType"],
                            id=attachment["id"],
                            size=attachment["size"],
                            stored_filename=attachment["storedFilename"],
                        )
                        for attachment in data_message.get("attachments", [])
                    ],
                )
            elif data.get('receipt'):
                msg.has_content = False
        return msg

    def receive_messages(self, json_print=False) -> Iterator[Message]:
        s = self._get_socket()
        s.send(json.dumps({"type": "subscribe", "username": self.username}).encode("utf8") + b"\n")

        for line in readlines(s):
            try:
                message = json.loads(line.decode())
                if json_print:
                    print(message)
            except json.JSONDecodeError:
                print("Invalid JSON")

            msg = self.serializeMessages(message)
            yield msg

    def chat_handler(self, regex, order=100):
        """
        A decorator that registers a chat handler function with a regex.
        """
        if not isinstance(regex, RE_TYPE):
            regex = re.compile(regex, re.I)

        def decorator(func):
            self._chat_handlers.append((order, regex, func))
            # Use only the first value to sort so that declaration order doesn't change.
            self._chat_handlers.sort(key=lambda x: x[0])
            return func

        return decorator

    def handle_chat(self, msg_print):
        def decorator(func):
            self._handle_messages.append((func, msg_print))
            return func
        return decorator

    def run(self):
        for message in self.receive_messages():
            for func, msg_print in self._handle_messages:
                if msg_print:
                    print(message)
                if message.has_content:
                    if not message.timestamp:
                        continue
                    if message.timestamp < self.start_time:
                        continue
                    func(message)
