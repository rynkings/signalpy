import attr


@attr.s
class Attachment:
    content_type = attr.ib(type=str)
    id = attr.ib(type=str)
    size = attr.ib(type=int)
    stored_filename = attr.ib(type=str)

@attr.s
class Group:
    group_id = attr.ib(type=str)
    group_name = attr.ib(type=str)
    group_type = attr.ib(type=str)

@attr.s
class GroupV2:
    group_id = attr.ib(type=str)

@attr.s
class Quote:
    id = attr.ib(type=str)
    author = attr.ib(type=str)
    text = attr.ib(type=str)

@attr.s
class Message:
    type = attr.ib(type=str, default=None)
    username = attr.ib(type=str, default=None)
    source = attr.ib(type=str, default=None)
    sender = attr.ib(type=str, default=None)
    sender_uuid = attr.ib(type=str, default=None)
    text = attr.ib(type=str, default=None)
    to = attr.ib(type=str, default=None)
    client_type = attr.ib(type=str, default=None)
    source_device = attr.ib(type=int, default=0)
    timestamp = attr.ib(type=int, default=None)
    timestamp_iso = attr.ib(type=str, default=None)
    expiration_secs = attr.ib(type=int, default=0)
    attachments = attr.ib(type=list, default=[])
    quote = attr.ib(type=str, default=None)
    group_info = attr.ib(type=dict, default={})
    isGroup = attr.ib(type=bool, default=False)
    group = attr.ib(default=None)
    groupV2 = attr.ib(default=None)
    quote = attr.ib(default=None)
    has_content = attr.ib(type=bool, default=False)