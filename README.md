signalpy
=======

signalpy is modified file from [pysignal](https://gitlab.com/stavros/pysignald).

signalpy is a Python client for the excellent [signald](https://git.callpipe.com/finn/signald) project, which in turn
is a command-line client for the Signal messaging service.

signalpy allows you to programmatically send and receive messages to Signal.

NOTE: Unfortunately, this library might be somewhat out of date or parts of it might not be working, as the upstream API
keeps changing, breaking compatibility. If you notice any breakage, MRs to fix it would be appreciated.

Documentation [signald](https://git.callpipe.com/finn/signald)

Installation
------------

1. Install signald or build from source
    ```bash
    $ git clone https://gitlab.com/signald/signald.git
    $ cd signald
    $ make installDist
    $ make setup
    ```

Running
-------

You need to run the signald first

```
cd signald
build/install/signald/bin/signald
```

Just make sure you have signald installed. Here's an example of how to use signalpy:

```python
from signalpy import Signal

s = Signal("+1234567890")

# If you haven't registered/verified signald, do that first:
s.register(voice=False)
s.verify("sms code")

s.sendMessage("+1098765432", "Hello there!")

for message in s.receive_messages():
    print(message)
```

You can also use the chat decorator interface:

```python
from signald import Signal

s = Signal("+1234567890")

@s.chat_handler()  # This is case-insensitive.
def handle_message(message):
    # Returning `False` as the first argument will cause matching to continue
    # after this handler runs.
    print(message)

s.run()
```

You can also login with qr code:

```python
from signald import Signal

s = Signal("qr")
s.login_qr('Ryns', 'session')

@s.chat_handler()  # This is case-insensitive.
def handle_message(message):
    # Returning `False` as the first argument will cause matching to continue
    # after this handler runs.
    print(message)

s.run()
```

Various
-------

signalpy also supports different socket paths:

```python
s = Signal("+1234567890", socket_path="/var/some/other/socket.sock")
```

It supports TCP sockets too, if you run a proxy. For example, you can proxy signald's UNIX socket over TCP with socat:

```bash
$ socat -d -d TCP4-LISTEN:15432,fork UNIX-CONNECT:/var/run/signald/signald.sock
```

Then in pysignald:

```python
s = Signal("+1234567890", socket_path=("your.serveri.ip", 15432))
```
